$(function () {
orszagLista();
        /*
         TODO:
         az oldal betöltésekor lekérni a /rest/countries URL-ről az országok listáját,
         és a város hozzáadása popup <select> elemébe berakni az <option>-öket
         */

        function orszagLista() {
        $.ajax({type: "GET",
                url: "/rest/countries",
                async: true,
                success: function (countries) {
                for (var i = 0; i < countries.length; i++) {
                $('#city-country').contents().val(countries[i]);
                }


                }});
        }

        $('body').on('click', '#add-city-button', function () {
        /*
         TODO: valamiért ez az eseménykezelő le sem fut...
         */
            var cityData = {

                name = $('#city-name').val(),
                population = $('#city-population').val(),
                country = $('#city-country').val()
                /*
                 TODO: összeszedni az űrlap adatait
                 */
            };
                $.ajax({
                url: '/rest/cities',
                        method: 'POST',
                        data: cityData,
                        dataType: 'json',
                        complete: function () {

                        $('#add-city-modal').hide();
                                /*
                                 TODO: Be kellene csukni a popupot
                                 */
                        }
                });
        });
                /*
                 TODO: ország törlés gombok működjenek
                 */
        $('#countryDelete').on('click', function () {
            if (confirm("Biztos törölni akarod?")) {
                $.ajax({
                        method: 'DELETE',
                        url: 'http://localhost:8080/',
                        data: {
                            id: $(this).closest('tr').attr('data-id')
                        },
                        success: function(data) {
                                $(this).closest('tr').remove()
                        }
                })
            }
        });

                /*
                 TODO: város törlés gombok működjenek
                 */

                });